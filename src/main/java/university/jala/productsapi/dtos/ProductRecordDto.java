package university.jala.productsapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record ProductRecordDto(@NotBlank String category, @NotBlank String name, String description, @NotNull int quantity, @NotNull BigDecimal value) {
}
