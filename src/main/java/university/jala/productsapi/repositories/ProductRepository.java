package university.jala.productsapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import university.jala.productsapi.models.ProductModel;
import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<ProductModel, UUID> {

    @Query("SELECT DISTINCT p.category FROM ProductModel p")
    List<String> findAllCategories();

    List<ProductModel> findByCategory(String category);

    List<ProductModel> findByQuantityGreaterThanEqual(int quantity);
}
