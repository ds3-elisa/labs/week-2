package university.jala.productsapi.controller;

import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.productsapi.dtos.ProductRecordDto;
import university.jala.productsapi.models.ProductModel;
import university.jala.productsapi.service.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> saveProduct(@RequestBody @Valid ProductRecordDto productRecordDto) {
        ProductModel productModel = productService.createProduct(productRecordDto).getBody();
        return ResponseEntity.status(HttpStatus.CREATED).body(productModel);
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getAllProducts() {
        List<ProductModel> productList = productService.getAllProducts().getBody();
        return ResponseEntity.status(HttpStatus.OK).body(productList);
    }

    @GetMapping("/products/in-stock")
    public ResponseEntity<List<ProductModel>> getProductsByStockQuantity(@RequestParam("quantity") int quantity) {
        List<ProductModel> productList = productService.getProductsByStockQuantity(quantity);
        return ResponseEntity.status(HttpStatus.OK).body(productList);
    }

    @GetMapping("/products/categories")
    public ResponseEntity<List<String>> getAllCategories() {
        List<String> categories = productService.getAllCategories();
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }

    @GetMapping("/products/categories/{category}")
    public ResponseEntity<List<ProductModel>> getProductsByCategory(@PathVariable("category") String category) {
        List<ProductModel> productList = productService.getProductsByCategory(category);
        return ResponseEntity.status(HttpStatus.OK).body(productList);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getOneProduct(@PathVariable("id") UUID id) {
        return productService.getOneProduct(id);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable(value = "id") UUID id,
                                                @RequestBody @Valid ProductRecordDto productRecordDto) {

        ProductModel updatedProduct = productService.updateProduct(id, productRecordDto);
        if (updatedProduct == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product Not Found");
        }
        return ResponseEntity.ok(updatedProduct);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable(value="id") UUID id) {
        boolean deleted = productService.deleteProduct(id);
        if (!deleted) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product Not Found");
        }
        return ResponseEntity.status(HttpStatus.OK).body("Product Deleted Successfully");
    }
}
