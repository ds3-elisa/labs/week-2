package university.jala.productsapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import university.jala.productsapi.controller.ProductController;
import university.jala.productsapi.dtos.ProductRecordDto;
import university.jala.productsapi.models.ProductModel;
import university.jala.productsapi.repositories.ProductRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ResponseEntity<ProductModel> createProduct(ProductRecordDto productRecordDto) {
        var productModel = new ProductModel();
        BeanUtils.copyProperties(productRecordDto, productModel);
        ProductModel savedProduct = productRepository.save(productModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    public ResponseEntity<List<ProductModel>> getAllProducts() {
        List<ProductModel> productList = productRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(productList);
    }

    public List<ProductModel> getProductsByStockQuantity(int quantity) {
        return productRepository.findByQuantityGreaterThanEqual(quantity);
    }

    public List<String> getAllCategories() {
        return productRepository.findAllCategories();
    }

    public List<ProductModel> getProductsByCategory(String category) {
        return productRepository.findByCategory(category);
    }

    public ResponseEntity<Object> getOneProduct(UUID id) {
        Optional<ProductModel> productO = productRepository.findById(id);

        if (productO.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product Not Found");
        }

        ProductModel product = productO.get();
        Link link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ProductController.class).getAllProducts()).withRel("allProducts");
        product.add(link);

        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    public ProductModel updateProduct(UUID id, ProductRecordDto productRecordDto) {
        Optional<ProductModel> productOptional = productRepository.findById(id);

        if (productOptional.isEmpty()) {
            return null;
        }

        ProductModel productModel = productOptional.get();
        BeanUtils.copyProperties(productRecordDto, productModel);
        return productRepository.save(productModel);
    }

    public boolean deleteProduct(UUID id) {
        Optional<ProductModel> productOptional = productRepository.findById(id);

        if (productOptional.isEmpty()) {
            return false;
        }

        productRepository.delete(productOptional.get());
        return true;
    }
}