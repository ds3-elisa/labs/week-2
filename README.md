# Products API
### Uma API REST feita em Spring Boot

A Products API é uma interface de programação construída utilizando o framework Spring Boot e hospedada com o banco de dados PostgreSQL online através da ferramenta Neon Serverless Postgres.

As ferramentas utilizadas para o desenvolvimento da API foram:
- Spring Boot e o Spring Initializr
- IntelliJ Ultimate como IDE
- Postman
- Neon Serverless


## Requisitos funcionais
- **Inclusão de Produtos:** A API deve fornecer um endpoint para adicionar novos produtos ao estoque.
- **Exclusão de Produtos:** Deve ser possível excluir produtos do estoque através do id.
- **Alteração de Produtos:** A API deve permitir a atualização dos detalhes de um produto existente através do id.
- **Consulta de Produtos:** Deve ser possível consultar o estoque de produtos de diversas maneiras. Os usuários devem poder listar todos os produtos, buscar por categoria, disponibilidade em estoque e id.

## Arquitetura
- **Camada de Controller:** Responsável por receber as requisições HTTP, validar os dados de entrada e encaminhar as chamadas para os serviços apropriados.
- **Camada de Service:** Contém a lógica de negócios da aplicação. Aqui, as operações de inclusão, exclusão, alteração e consulta de produtos devem ser implementadas.
- **Camada de Repository:** Responsável pela interação com o banco de dados ou o Map Java que armazena os dados do estoque. Todas as operações de acesso aos dados devem ser encapsuladas nesta camada.

## Ferrementas

- [Spring Boot](https://spring.io/projects/spring-boot) - Framework Java.
- [Spring Initializr](https://start.spring.io/) - Gerador de projetos Spring.
- [IntelliJ Ultimate](https://www.jetbrains.com/pt-br/idea/) - IDE Java.
- [Postman](https://www.postman.com/) - Aplicação para teste de APIs.
- [Neon Serverless](https://neon.tech/) - Versão em nuvem do PostgreSQL.